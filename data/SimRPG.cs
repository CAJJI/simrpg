using System;
using System.Collections.Generic;
using System.IO;


public class Armour
{
    public string name;
    public int defense;
}

public class SimRPG
{
    public List<Armour> armour;
    
    void Build()
    {
        armour = new List<Armour>();
        CreateArmour("Steel Armour", 10);
    }

    void CreateArmour(string name, int defense)
    {
        armour.Add(new Armour() { name = name, defense = defense });
    }
}
