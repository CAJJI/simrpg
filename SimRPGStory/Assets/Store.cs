﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Store : StateMachineBehaviour {
    public WeaponId[] weapons;
    public HelmetId[] helmets;
    public UpperArmourId[] uppers;
    public LowerArmourId[] lowers;    
    public GlovesId[] gloves;
    public BootsId[] boots;

}
