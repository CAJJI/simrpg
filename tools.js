methods = {
    RandomInt: function (min, max) {
        return min + Math.floor(Math.random() * Math.floor(max));
    },
    Roll: function(){
        return this.RandomInt(1,6);
    }
    ,
    ToString: function(any){
        return any+='';
    },
    NewLine: function(lines){
        var string = ""
        for (line = 0; line < lines; line++)
        string += '\n';
        return string;
    },
    BoxText:function(string){        
        return "```" + this.NewLine(1) + string + "```";
    },
    GetCommand: function(text, command){    
        return text.startsWith(command);
    },
    ReEvaluateArray:function(array){
        var current = 0;
        var newArray = [];
        for (var a = 0; a < array.length; a++){
            if (array[a]){
                newArray[current] = array[a];
                current++;
            }
        }
        return newArray;
    },
    GetHealth:function(character){
        return "(" + character.health + "/" + character.maxHealth +")";
    },
    GetHealthBar:function(character){
        var string = "[";
        var percent = (character.health/character.maxHealth) * 10;
        for (var i = 0; i < percent; i++){
            string += "|";
        }
        for (var y = percent; y < 10; y++){
            string += " ";
        }
        string += "]";        
        return string;
    },    
}

module.exports = methods