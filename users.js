const tools = require('./tools.js');
const data = require('./data/userData.json');
const room = require('./room.js');

const fs = require('fs');

methods = {
    
    CreateCharacter: function(id, name){                                        
        data[id] = {
            id: id,
            name: name,
            level: 1,
            dead: false,   
            maxHealth : 100,         
            health: 100,
            stateId: 0,
            previousStateId: 0,
            battleId: null,
            gold: 100,
            weapon: null,
            helmet: null,
            upper: null,
            lower: null,
            gloves: null,
            boots: null,
        }                       
        this.Save();
    },
        
    GetCharacter: function(id){
        return data[id];
    },

    GetInfo: function(character){
        var s = '';
        s += "name: " + character.name + tools.NewLine(1);
        s += "level: " + character.level + tools.NewLine(1);
        s += "health: " + character.health + tools.NewLine(1);
        s += "gold: " + character.gold + tools.NewLine(1);
        s += "weapon: " + this.GetItemName(character.weapon) + tools.NewLine(1);
        s += "helmet: " + this.GetItemName(character.helmet) + tools.NewLine(1);
        s += "upper armour: " + this.GetItemName(character.upper) + tools.NewLine(1);
        s += "lower armour: " + this.GetItemName(character.lower) + tools.NewLine(1);
        s += "gloves: " + this.GetItemName(character.gloves) + tools.NewLine(1);
        s += "boots: " + this.GetItemName(character.boots) + tools.NewLine(1);
        return s;
    },

    Kill:function(character){
        character.dead = true;
    },

    GetItemName:function(item){
        if (item){
            return item.name;
        }
        return "none";
    },

    UpdateState : function(id, stateId){
        var character = this.GetCharacter(id);
        character.previousStateId = character.stateId;
        character.stateId = stateId;
        this.Save();
    },

    Equip:function(character, item){        
        if (item.type === 0)
        character.weapon = item;
        if (item.type === 1)
        character.helmet = item;
        if (item.type === 2)
        character.upper = item;
        if (item.type === 3)
        character.lower = item;
        if (item.type === 4)
        character.gloves = item;
        if (item.type === 5)
        character.boots = item;                
        this.Save();
    },

    AdjustGold:function(character, amount){
        character.gold += amount;
        this.Save();        
    },

    Resurrect: function (msg, character) {
        character.health = character.maxHealth;
        character.dead = false;
    },

    Save : function(){         
        if (data){  
            fs.writeFile('./data/userData.json', JSON.stringify(data, null, 4), err => {
                if (err) throw err;
            });   
        }
        else
        console.log("corrupt user data.")     ;
    },
}

module.exports = methods;
