const fs = require('fs');
const data = require('./data/storyData.json');
const tools = require('./tools.js');
const sim = require('./simRPG.js');
const users = require('./users.js');
const room = require('./room.js');
const items = require('./items.js');
const battle = require('./battle.js');

methods = {    

    GetState : function(stateId){
        for (i = 0 ; i < data.states.length; i++){
            if (data.states[i].id === stateId)
            return data.states[i];
        }        
    },
    
    CheckInstance : function(msg, text){    
    var character = users.GetCharacter(msg.author.id)
    if (!character || character.dead) return;    
    var state = this.GetState(character.stateId);
    if (!state) return;        

    if (this.CheckBattle(msg,text,character,state)){
        this.Save();
        users.Save();
        return;    
    }

    this.CheckStore(msg, text, character, state);

    for (i = 0; i < state.responses.length; i++){        
        if (tools.GetCommand(text, state.responses[i])){       
            var destinationState = this.GetState(state.destinationIds[i]);  
            this.SetState(msg, destinationState)            
            return;
        }
    }    

    this.Save();
    users.Save();
},

CheckBattle:function(msg,text,character,state){        
    if (character.battleId !== null){
        if (tools.GetCommand(text, "attack")){
            battle.Attack(msg, character, state)
        }

        if (tools.GetCommand(text, "flee")){
            this.Flee(msg, character, state);                
        }
        return true;
    }
    return false;    
},

Flee:function(msg,character,state){
    var success = tools.RandomInt(0,100) < state.battle.activeInfo.avoidability;
    if (success){    
        room.SendMessage(msg, "You successfully flee.", true);        
        sim.states.SetState(msg, character.previousStateId);
    }
    else{
        var string = "You fail to flee.";
        string += battle.EnemyAttack(character);        

        if (!character.dead){
        string += tools.NewLine(2) + "What do you choose to do?" + tools.NewLine(1);            
        string += "[!flee] "
        string += "[!attack] "          
        }

        room.SendMessage(msg, string, true)
    }
},

CheckStore:function(msg,text,character,state){
    if (state.isStore){
        if (tools.GetCommand(text, "buy")){
            var index = text.slice(4);            
            this.BuyItem(msg, character, state.store, index);
        }
    }    
},

DisplayStore:function(store){
    var string = tools.NewLine(1);    
    console.log(store.items.length);
    for (k = 0 ; k < store.items.length; k++){                        
        console.log(k);
        var item = items.GetItem(store.items[k].type, store.items[k].id);
        console.log(item);
        string += (k + 1) + ". " + item.name + " " + item.cost;
        string += tools.NewLine(1);        
        console.log(k);
    }
    return string;
},

GetItemFromStore :function(store, index){
    index -= 1;    
    var current = 0;
    for (i = 0; i < store.items.length; i++){        
        if (current === index){
            return items.GetItem(store.items[i].type, store.items[i].id);
        }
    current++;    
    }
},

BuyItem:function(msg, character, store, index){
    var item = this.GetItemFromStore(store, index);
    if (!item) return;
    if (character.gold >= item.cost){
    users.Equip(character, item);
    users.AdjustGold(character, -item.cost);
    room.SendMessage(msg, character.name + " has purchased " + item.name, true);
    }
    else{
        room.SendMessage(msg,  "Cannot afford " + item.name, true);
    }
},

SetState:function(msg, state){
    this.AssessState(state);                                       
    this.DisplayInstance(msg, state.id);            
    if (!state.temporary){        
        this.UpdateInstance(msg, state.id);
    }
},

UpdateInstance:function(msg, id){       
    users.UpdateState(msg.author.id, id)            
},

GetBattleResponses:function(msg,character,battleInfo){    
        string += tools.NewLine(2) + "What do you choose to do?" + tools.NewLine(1);            
        string += "[!flee] "
        string += "[!attack #] "          
        for (var i = 0; i < battleInfo.enemies; i++){
            //display enemies and info
        }
},

DisplayInstance:function(msg, id){
    var state = this.GetState(id);
    var string =  state.text;    
    
    //check if battle is in state, if so check flee or fight    

    if (state.isBattle){
        var character = users.GetCharacter(msg.author.id);
        string += tools.NewLine(2) + state.battle.activeInfo.text;        
        string += GetBattleResponses(msg,character, state.battle.activeInfo);
        battle.StartBattle(msg, users.GetCharacter(msg.author.id), state); 
        room.SendMessage(msg,string,true);      
    return;
    }    

    string += tools.NewLine(2) + "What do you choose to do?" + tools.NewLine(1);            

    for (i = 0; i < state.responses.length; i++){
    string += "[!" + state.responses[i] + "] ";
    }    

    if (state.isStore){
    string += "[!buy #] ";
    string += tools.NewLine(1);
    string += this.DisplayStore(state.store);
    }
    //CheckBattle(state);        
    room.SendMessage(msg,string,true);        
},

AssessState : function(state){
    //if (!state.isBattle){        
    state.isBattle = false;
    state.battle.activeInfo = null;

        var total = 0;
        var noBattle = 0;        
        for (x = 0; x < state.battle.infos.length; x++){            
            if (!state.battle.infos[x].defeated || state.battle.infos[x].respawnable){
            noBattle += 100 - state.battle.infos[x].likelihood;
            total += state.battle.infos[x].likelihood;            
            }
        }
        var current = 0;
        var random = tools.RandomInt(0, total + noBattle);
        for (z = 0 ; z < state.battle.infos.length; z++){
            if (!state.battle.infos[z].defeated || state.battle.infos[z].respawnable){
            current += state.battle.infos[z].likelihood
            if (random < current){
                state.battle.activeInfo = state.battle.infos[z];
                state.isBattle = true;
                break;
            }
        }
    }        
    },

Save : function(){    
    if (data){  
        fs.writeFile('./data/storyData.json', JSON.stringify(data, null, 4), err => {
            if (err) throw err;
        });   
    }
    else
    console.log("corrupt story data.");
},
}

module.exports = methods;