﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CanEditMultipleObjects]
[CustomEditor(typeof(BattleState))]
public class BattleStateEditor : DefBase
{
    ReorderableList infos;

    public override void OnInspectorGUI()
    {
        BattleState target = (BattleState)this.target;

        DrawInfos(target);
        serializedObject.ApplyModifiedProperties();
    }

    private void OnEnable()
    {
        infos = new ReorderableList(serializedObject, serializedObject.FindProperty("infos"), true, true, true, true);
    }

    void DrawInfos(BattleState target)
    {
        EditorStyles.textField.wordWrap = true;        
        infos.elementHeightCallback = (int index) =>
        {
            if (!infos.serializedProperty.isExpanded) return 0;
            float height = EditorGUIUtility.singleLineHeight * 1.5f;            
            SerializedProperty element = infos.serializedProperty.GetArrayElementAtIndex(index);
            SerializedProperty enemyInfo = element.FindPropertyRelative("enemyInfo");
            if (enemyInfo.isExpanded)
            {
                height += 60;
                height += 20 * enemyInfo.arraySize;
            }

            return height;
        };

        infos.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            //draw battle info
            //for each battleenemyinfo, draw id and amount

            rect.y += 2;

            SerializedProperty element = infos.serializedProperty.GetArrayElementAtIndex(index);
            SerializedProperty text = element.FindPropertyRelative("text");
            SerializedProperty likelihood = element.FindPropertyRelative("likelihood");
            SerializedProperty enemyInfo = element.FindPropertyRelative("enemyInfo");

            enemyInfo.isExpanded = EditorGUI.Foldout(RectOffset(rect, 10, 0, 1, 1, false), enemyInfo.isExpanded, "");

            if (EditorGUI.ToggleLeft(RectOffset(rect, 120,0,30,1,false),"+", false))            
                enemyInfo.InsertArrayElementAtIndex(enemyInfo.arraySize);

            if (EditorGUI.ToggleLeft(RectOffset(rect, 150, 0, 30, 1, false),"-", false))
                enemyInfo.DeleteArrayElementAtIndex(enemyInfo.arraySize-1);

            serializedObject.ApplyModifiedProperties();

            EditorGUI.LabelField(RectOffset(rect, 10, 0, 150, 1, false), "Likelihood");
            likelihood.intValue = EditorGUI.IntField(RectOffset(rect, 80, 0, 30, 1, false), likelihood.intValue);            

            if (enemyInfo.isExpanded)
            {
                text.stringValue = EditorGUI.TextArea(RectOffset(rect, 10, 20, Screen.width * 0.8f, 3, false), text.stringValue);

                for (int i = 0; i < enemyInfo.arraySize; i++)
                {
                    int heightOffset = 20;
                    DrawEnemyBattleInfo(target.infos[index].enemyInfo[i], rect, 50 + (heightOffset * (i + 1)), enemyInfo.GetArrayElementAtIndex(i));
                }
            }
        };

        infos.serializedProperty.isExpanded = DrawItem(infos, "EnemyInfos", infos.serializedProperty.isExpanded);
    }

    public void DrawEnemyBattleInfo(BattleEnemyInfo target, Rect rect, int heightOffset, SerializedProperty enemy)
    {
        SerializedProperty id = enemy.FindPropertyRelative("id");
        SerializedProperty amount = enemy.FindPropertyRelative("amount");

        id.enumValueIndex = (int)(EnemyId)EditorGUI.EnumPopup(RectOffset(rect, 10, heightOffset, 100, 1, false), target.id);

        EditorGUI.LabelField(RectOffset(rect, 120, heightOffset, 100, 1, false), "Amount");
        amount.intValue = EditorGUI.IntField(RectOffset(rect, 170, heightOffset, 100, 1, false), amount.intValue);
    }
}

[System.Serializable]
public class BattleEnemyInfo
{
    public EnemyId id;    
    public int amount = 1;
}

[System.Serializable]
public class BattleInfo
{
    public bool respawnable;
    public bool defeated;
    public int avoidability = 50;
    public int likelihood;
    public string text;
    public BattleEnemyInfo[] enemyInfo;
    public Enemy[] enemies;
}

public class BattleState : StateMachineBehaviour
{
    public BattleInfo[] infos;
}