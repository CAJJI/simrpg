﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
using UnityEditorInternal;
using UnityEditor.Animations;
using UnityEngine.Animations;

[CanEditMultipleObjects]
[CustomEditor(typeof(DialogueState))]
public class DialogueStateEditor : Editor
{
    ReorderableList list;

    public override void OnInspectorGUI()
    {
        DialogueState dialogueState = (DialogueState)target;

        GUILayout.Label("Character");
        dialogueState.characterType = (DialogueCharacterType)EditorGUILayout.EnumPopup(dialogueState.characterType, GUILayout.Width(Screen.width / 3));
        serializedObject.FindProperty("characterType").enumValueIndex = (int)dialogueState.characterType;    


            GUILayout.Label("Dialogue");
        serializedObject.FindProperty("text").stringValue = GUILayout.TextArea(dialogueState.text, GUILayout.MinHeight(EditorGUIUtility.singleLineHeight * 3));

        list.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Responses");
        };
        
        list.elementHeightCallback = (int index) =>
        {
            float height = EditorGUIUtility.singleLineHeight * 2f;
            //if (list.index == index) height *= 2;
            return height;
        };

        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.LabelField(new Rect(rect.x, rect.y, 30, EditorGUIUtility.singleLineHeight), index.ToString());
            EditorGUI.PropertyField(new Rect(rect.x + 30, rect.y, Screen.width * 0.6f, EditorGUIUtility.singleLineHeight), element, GUIContent.none);
        };
        list.DoLayoutList();

        serializedObject.ApplyModifiedProperties();

    }

    private void OnEnable()
    {
        list = new ReorderableList(serializedObject, serializedObject.FindProperty("responses"), true, true, true, true);
    }
}

#endif
public enum DialogueCharacterType
{
    Source,
    Other
}

public class DialogueState : StateMachineBehaviour {

    public bool isActive;
    public bool isEnd;


    //Add arrays of text for randomized variety of text "what's up", "how's it going", etc
    //Create a new text array for EACH way of speaking so that all ways of talking can be contained in the state and not excel
    public string text;
    public DialogueCharacterType characterType;
    
    public string[] responses;

#if UNITY_EDITOR

    public void ClearConditions()
    {
        foreach(AnimatorStateTransition transition in GetState().transitions)
        {
            transition.conditions = new AnimatorCondition[0];
        }
        Debug.Log("Conditions cleared");
    }

    public void AddChoiceToTransitions()
    {
        int current = 0;
        foreach (AnimatorStateTransition transition in GetState().transitions)
        {            
            if (ContainsCondition(transition, "choice")) continue;

            Debug.Log(transition.destinationState.name + " = choice " + current);

            transition.AddCondition(AnimatorConditionMode.Equals, current, "choice");
            current++;
        }
    }

    public void AddContinueToTransitions()
    {
        foreach (AnimatorStateTransition transition in GetState().transitions)
        {
            if (ContainsCondition(transition, "continue")) continue;
            
            transition.AddCondition(0, 0, "continue");
            Debug.Log("Continue added to " + transition.destinationState.name);
        }
    }


    public bool ContainsCondition(AnimatorStateTransition transition, string name)
    {
        foreach (AnimatorCondition condition in transition.conditions)
        {
            if (condition.parameter == name) return true;
        }
        return false;
    }


    public AnimatorState GetState()
    {
        UnityEditor.Animations.AnimatorController animator = GetAnimator();        

        foreach (ChildAnimatorState state in animator.layers[0].stateMachine.states)
        {
            foreach (StateMachineBehaviour behaviour in state.state.behaviours)
            {
                if (behaviour == this)
                {
                    return state.state;
                }
            }
        }
        return null;
    }

    public UnityEditor.Animations.AnimatorController GetAnimator()
    {
        StateMachineBehaviourContext[] contexts = UnityEditor.Animations.AnimatorController.FindStateMachineBehaviourContext(this);        
        foreach (StateMachineBehaviourContext context in contexts)
        {            
            foreach (State state in context.animatorController.GetBehaviours<State>())
            {                
                if (state == this)
                    return context.animatorController;
            }
        }
        return null;
    }

#endif
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
 

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        isActive = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    
    }
}
