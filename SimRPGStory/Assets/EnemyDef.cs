﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CanEditMultipleObjects]
[CustomEditor(typeof(EnemyDef))]
public class EnemyDefEditor: DefBase
{
    ReorderableList enemies;
    public override void OnInspectorGUI()
    {
        EnemyDef target = (EnemyDef)this.target;

        target.fileName = GUILayout.TextField(target.fileName);
        if (GUILayout.Button("Export"))
        {
            StreamWriter sw = StoryExporter.GetFile(StoryExporter.GetPath(target.fileName));

            foreach(Enemy enemy in target.enemies)
            {
                enemy.name = StoryExporter.GetName(enemy.id, typeof(EnemyId));
            }

            Debug.Log(StoryExporter.WriteToFile(sw, target));
        }
        DrawEnemies(target);

        serializedObject.ApplyModifiedProperties();
    }

    private void OnEnable()
    {
        enemies = new ReorderableList(serializedObject, serializedObject.FindProperty("enemies"), true, true, true, true);
    }

    void DrawEnemies(EnemyDef target)
    {
        enemies.elementHeightCallback = (int index) =>
        {
            if (!enemies.serializedProperty.isExpanded) return 0;
            float height = EditorGUIUtility.singleLineHeight * 1.5f;
            return height;
        };

        enemies.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            if (enemies.serializedProperty.isExpanded)
            {
                rect.y += 2;
                SerializedProperty element = enemies.serializedProperty.GetArrayElementAtIndex(index);
                SerializedProperty id = element.FindPropertyRelative("id");
                SerializedProperty health = element.FindPropertyRelative("health");
                SerializedProperty maxHealth = element.FindPropertyRelative("maxHealth");
                SerializedProperty minDamage = element.FindPropertyRelative("minDamage");
                SerializedProperty maxDamage = element.FindPropertyRelative("maxDamage");
                EditorGUI.LabelField(RectOffset(rect, 0, 0, 30, 1, false), index.ToString());

                id.enumValueIndex = (int)(EnemyId)EditorGUI.EnumPopup(RectOffset(rect, 0.05f, 0, 0.3f, 1, true), target.enemies[index].id);

                EditorGUI.LabelField(RectOffset(rect, 0.37f, 0, 0.1f, 1, true), "Health");
                health.intValue = EditorGUI.IntField(RectOffset(rect, 0.45f, 0, 0.1f, 1, true), health.intValue);
                maxHealth.intValue = health.intValue;

                EditorGUI.LabelField(RectOffset(rect, 0.56f, 0, 0.1f, 1, true), "Damage");
                Vector2 damage = EditorGUI.Vector2Field(RectOffset(rect, 0.66f, 0, 0.2f, 1, true), "", new Vector2(minDamage.intValue, maxDamage.intValue));
                minDamage.intValue = (int)damage.x;
                maxDamage.intValue = (int)damage.y;
            }
        };
        enemies.serializedProperty.isExpanded = DrawItem(enemies, "Enemies", enemies.serializedProperty.isExpanded);
    }    
}

[CreateAssetMenu(fileName = "EnemyDef", menuName = "RPG/EnemyDef")]
public class EnemyDef : ScriptableObject {

    public string fileName = "enemyData";
    public Enemy[] enemies;
}
