const fs = require('fs');
const states = require('./states.js');
const data = require('./data/battleData.json');
const users = require('./users.js');
const tools = require('./tools.js');
const room = require('./room.js');
const enemies = require('./enemies.js');

methods = {


    //make it so player can choose which enemy to attack
    //figure out why states.Save() and states.DisplayInstance() isnt working
    //add death state
    //check victory state
    //check likeliness and fix avoidability
    //add party battles
    //add flee

    GetBattle(battleId){      
            return data[battleId];        
    },

    StartBattle:function(msg, character, state){
        var id = tools.RandomInt(1000,9999);
        character.battleId = id;
        users.Save();
        var battleInfo = state.battle.activeInfo;
        var currentEnemy =0;
        for (l = 0; l < battleInfo.enemyInfo.length;l++){                        
            var amount = battleInfo.enemyInfo[l].amount;
            for (m = 0; m < amount; m++){                                
                battleInfo.enemies[currentEnemy] = enemies.GetEnemy(battleInfo.enemyInfo[l].id);                
                currentEnemy++;
            }
        }                
        data[id] = {            
            character : character,
            info: battleInfo,
        }        
        this.Save();
        //this.Attack(msg, character, state);
    },

    Attack:function(msg, character, state){        
        //var roll = tools.Roll();
        var string = "";
        var battleInfo = this.GetBattle(character.battleId).info;        
        var random = tools.RandomInt(0, battleInfo.enemies.length-1);
        var enemy = battleInfo.enemies[random];
        var weapon = character.weapon;
        var damage = 1;        
        if (weapon)
            damage = tools.RandomInt(weapon.minDamage,weapon.maxDamage);

            var weaponName = "their fist";
            if (weapon) 
            weaponName = character.weapon.name;
                                        
        var string = character.name + " attacks " + enemy.name + " with " + weaponName + " for " + damage + " damage.";
        enemy.health -= damage;                
        if (enemy.health <= 0){
            string += tools.NewLine(1);
            string += enemy.name + " has been killed.";
            battleInfo.enemies[random] = null;
            battleInfo.enemies = tools.ReEvaluateArray(battleInfo.enemies);            
        }                        

        if (battleInfo.enemies.length === 0){
            string += tools.NewLine(1);
            string += "Battle has been won!";
            room.SendMessage(msg, string,true);
            this.EndBattle(msg, character, state);
        }        
        
        else{                     
            string += this.EnemyAttack(character);
            string += tools.NewLine(1);                    
            this.DisplayBattle(msg,string,character);
        }                        

        users.Save();
        //states.Save();
        this.Save();        
    },

    EnemyAttack:function(character){        
        var string = "";
        var battleInfo = this.GetBattle(character.battleId).info;            
        var random = tools.RandomInt(0, battleInfo.enemies.length-1);
        var enemy = battleInfo.enemies[random];
        var damage = tools.RandomInt(enemy.minDamage,enemy.maxDamage);
        string += tools.NewLine(2);
        string += enemy.name + " attacks " + character.name + " for " + damage + " damage.";

        character.health -= damage;
        if (character.health <= 0){     
            //character.health = 100;       
            users.Kill(character);
            string += tools.NewLine(2) + character.name + " has died.";            
        }
        return string;
    },

    DisplayBattle:function(msg,string,character){        
        if (!character.dead){
        string += tools.NewLine(2) + "What do you choose to do?" + tools.NewLine(1);            
        string += "[!flee] "
        string += "[!attack] "     
        }
        room.SendMessage(msg, string,true);
    },

    EndBattle:function(msg, character, state){
        //this.GetBattle(character.battleId) = null;
        data[character.battleId] = null;
        character.battleId = null;        
        state.battle.activeInfo = null;        
        states.DisplayInstance(msg, state);
    },    

    Save:function(){
        if (data){  
            fs.writeFile('./data/battleData.json', JSON.stringify(data, null, 4), err => {
                if (err) throw err;
            });   
        }
        else
        console.log("corrupt battle data.");
    }
}

module.exports = methods;