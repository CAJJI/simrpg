const data = require('./data/itemData.json')

methods = {
    GetItem : function(type, id){
        for (i = 0 ; i < data.items.length; i++){
            if (data.items[i].type === type){
            if (data.items[i].id === id)
                return data.items[i];
            }
        }        
    }
}
module.exports = methods;