﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Animations;

public class StateBase : StateMachineBehaviour {

    public bool ContainsCondition(AnimatorStateTransition transition, string name)
    {
        foreach (AnimatorCondition condition in transition.conditions)
        {
            if (condition.parameter == name) return true;
        }
        return false;
    }

    public AnimatorState GetState()
    {
        UnityEditor.Animations.AnimatorController animator = GetAnimator();

        foreach (ChildAnimatorState state in animator.layers[0].stateMachine.states)
        {
            foreach (StateMachineBehaviour behaviour in state.state.behaviours)
            {
                if (behaviour == this)
                {
                    return state.state;
                }
            }
        }
        return null;
    }

    public UnityEditor.Animations.AnimatorController GetAnimator()
    {
        StateMachineBehaviourContext[] contexts = UnityEditor.Animations.AnimatorController.FindStateMachineBehaviourContext(this);
        foreach (StateMachineBehaviourContext context in contexts)
        {
            foreach (StateBase state in context.animatorController.GetBehaviours<StateBase>())
            {
                if (state == this)
                    return context.animatorController;
            }
        }
        return null;
    }
}
