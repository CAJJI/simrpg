﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    Weapon,
    Helmet,
    UpperArmour,
    LowerArmour,
    Gloves,
    Boots,
}

public enum WeaponId
{
    SteelDagger,
    SteelSword,                
}

public enum HelmetId
{
    LeatherHeadband,
    SteelHelmet,
}

public enum UpperArmourId
{
    LeatherTunic,
    SteelChestPlate,
}

public enum LowerArmourId
{
    LeatherPants,
    SteelLeggings,
}

public enum GlovesId
{
    LeatherGloves,
    SteelGloves,
}

public enum BootsId
{
    LeatherBoots,
    SteelBoots,
}

[System.Serializable]
public class ExportItem
{
    public ItemType type;
    public int id;
    public string name;
    public int cost;
    public int minDamage;
    public int maxDamage;
    public int defense;

    public static ExportItem[] GetItems(Item[] items)
    {
        List<ExportItem> list = new List<ExportItem>();
        foreach(Item item in items)
        {
            list.Add(GetItem(item));
        }
        return list.ToArray();
    }

    public static ExportItem GetItem(Item item)
    {
        ExportItem exportItem = new ExportItem();

        exportItem.cost = item.cost;

        if (item is Weapon)
        {
            Weapon itemType = item as Weapon;            
            exportItem.type = ItemType.Weapon;
            exportItem.id = (int)itemType.id;
            exportItem.name = StoryExporter.GetName(itemType.id, typeof(WeaponId));
            exportItem.minDamage = itemType.minDamage;
            exportItem.maxDamage = itemType.maxDamage;
        }
        if (item is Helmet)
        {
            Helmet itemType = item as Helmet;
            exportItem.type = ItemType.Helmet;
            exportItem.name = StoryExporter.GetName(itemType.id, typeof(HelmetId));
            exportItem.id = (int)itemType.id;
            exportItem.defense = itemType.defense;
            
        }
        if (item is UpperArmour)
        {
            UpperArmour itemType = item as UpperArmour;
            exportItem.type = ItemType.UpperArmour;
            exportItem.name = StoryExporter.GetName(itemType.id, typeof(UpperArmourId));
            exportItem.id = (int)itemType.id;
            exportItem.defense = itemType.defense;
        }
        if (item is LowerArmour)
        {
            LowerArmour itemType = item as LowerArmour;
            exportItem.type = ItemType.LowerArmour;
            exportItem.name = StoryExporter.GetName(itemType.id, typeof(LowerArmourId));
            exportItem.id = (int)itemType.id;
            exportItem.defense = itemType.defense;
        }
        if (item is Gloves)
        {
            Gloves itemType = item as Gloves;
            exportItem.type = ItemType.Gloves;
            exportItem.name = StoryExporter.GetName(itemType.id, typeof(GlovesId));
            exportItem.id = (int)itemType.id;
            exportItem.defense = itemType.defense;
        }
        if (item is Boots)
        {
            Boots itemType = item as Boots;
            exportItem.type = ItemType.Boots;
            exportItem.name = StoryExporter.GetName(itemType.id, typeof(BootsId));
            exportItem.id = (int)itemType.id;
            exportItem.defense = itemType.defense;
        }
        return exportItem;
    }
}

[System.Serializable]
public class Item
{    
    public int cost;
}

[System.Serializable]
public class Weapon : Item
{
    public WeaponId id;
    public int minDamage;
    public int maxDamage;
}

[System.Serializable]
public class Armour : Item
{
    public int defense;
}

[System.Serializable]
public class Helmet : Armour
{
    public HelmetId id;
}

[System.Serializable]
public class UpperArmour : Armour
{
    public UpperArmourId id;
}

[System.Serializable]
public class LowerArmour : Armour
{
    public LowerArmourId id;
}

[System.Serializable]
public class Gloves : Armour
{
    public GlovesId id;
}

[System.Serializable]
public class Boots : Armour
{
    public BootsId id;
}