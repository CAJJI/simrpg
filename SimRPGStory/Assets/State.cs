﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEditor.Animations;

[CanEditMultipleObjects]
[CustomEditor(typeof(State))]
public class StateEditor : Editor
{
    ReorderableList list;

    public override void OnInspectorGUI()
    {
        State state = (State)target;

        SerializedProperty temporary = serializedObject.FindProperty("temporary");
        SerializedProperty location = serializedObject.FindProperty("location");
        
        temporary.boolValue = GUILayout.Toggle(temporary.boolValue, "Temporary");
        location.enumValueIndex = (int)(Location)EditorGUILayout.EnumPopup((Location)location.enumValueIndex);

        GUILayout.Label("Dialogue");
        serializedObject.FindProperty("text").stringValue = GUILayout.TextArea(state.text, GUILayout.MinHeight(EditorGUIUtility.singleLineHeight * 3));

        list.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Responses");
        };

        list.elementHeightCallback = (int index) =>
        {
            float height = EditorGUIUtility.singleLineHeight * 2f;
            //if (list.index == index) height *= 2;
            return height;
        };

        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.LabelField(new Rect(rect.x, rect.y, 30, EditorGUIUtility.singleLineHeight), index.ToString());
            EditorGUI.PropertyField(new Rect(rect.x + 30, rect.y, Screen.width * 0.6f, EditorGUIUtility.singleLineHeight), element, GUIContent.none);
        };
        list.DoLayoutList();

        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("Add Conditions"))
        {
            state.AddChoiceToTransitions();
        }

        if (GUILayout.Button("Clear Conditions"))
        {
            state.ClearConditions();
        }

        if (GUILayout.Button("Clear Transitions"))
        {
            state.ClearTransitions();
        }
    }

    private void OnEnable()
    {
        list = new ReorderableList(serializedObject, serializedObject.FindProperty("responses"), true, true, true, true);
    }
}

public class State : StateBase
{

    public Location location;
    public bool temporary;
    public string text;
    public string[] responses;    

    public void ClearTransitions()
    {
        AnimatorState state = GetState();
        while (state.transitions.Length > 0)
            state.RemoveTransition(state.transitions[0]);
    }

    public void ClearConditions()
    {        
        foreach (AnimatorStateTransition transition in GetState().transitions)
        {
            transition.conditions = new AnimatorCondition[0];
        }
        Debug.Log("Conditions cleared");
    }

    public void AddChoiceToTransitions()
    {
        int current = 0;
        foreach (AnimatorStateTransition transition in GetState().transitions)
        {
            if (ContainsCondition(transition, "choice")) continue;

            Debug.Log(transition.destinationState.name + " = choice " + current);

            transition.AddCondition(AnimatorConditionMode.Equals, current, "choice");
            current++;
        }
    }
}