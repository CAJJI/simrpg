const Discord = require('discord.js');
const client = new Discord.Client();
 
const users = require('./users.js');
const states = require('./states.js');
const room = require('./room.js');
const tools = require('./tools.js');

const prefix = "!"

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);  
    //preix = room.GetUsername(client.user.id) + " ";
});

client.on('message', msg => {    

    if (!msg.content.startsWith(prefix)) return;                
    if (msg.author.bot) return;        
    var text = msg.content.slice(prefix.length);            

    var newCharacter = 'new';
    var getCharacter = 'me'; 
    var instance = 'instance';

    states.CheckInstance(msg, text);

    if (tools.GetCommand(text, instance)){
        var character = users.GetCharacter(msg.author.id);
        if (character){
            if (character.dead)
            room.SendMessage(msg, character.name + " is dead. Make a new character or get resurrected.", true);
            else
            states.DisplayInstance(msg, character.stateId);        
        }
    }
    
    if (tools.GetCommand(text, newCharacter)){        
        var name = text.slice(newCharacter.length + 1);        
        name = name.charAt(0).toUpperCase() + name.slice(1);
        if (name.length === 0)
        room.SendMessage(msg, "Please state a name.",false);
        else{
        users.CreateCharacter(msg.author.id, name);        
        room.SendMessage(msg, "New character created: " + name,false);        
        var character = users.GetCharacter(msg.author.id);
        states.DisplayInstance(msg, 0);
        }
    }

    if (tools.GetCommand(text, getCharacter)){        
        var character = users.GetCharacter(msg.author.id);
        var info = users.GetInfo(character);
        room.SendMessage(msg, info, true);
    }

    //Save()
    //character creation

    //get user character
    //if no character, say so    
    //get character instance

    //check text against instance options
    //update character instance (and all in party)
        
});

client.login('NTExNjk3MzM4MTE3NTIxNDE4.Dsuw_Q._MXs - GQIBIsGYM90gh9DyHp7nVo');