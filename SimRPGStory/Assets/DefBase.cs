﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

public class DefBase : Editor {

    public bool DrawItem(ReorderableList list, string name, bool expanded)
    {
        bool ret = false;
        list.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(RectOffset(rect, 20, 0, 200, 1, false), name);
            ret = (EditorGUI.Foldout(RectOffset(rect, 10, 0, 1, 1, false), expanded, ""));
        };

        list.DoLayoutList();
        return ret;
    }

    public Rect RectOffset(Rect rect, float xOffset, float yOffset, float width, float height, bool stretch)
    {
        if (stretch)
        {
            width *= Screen.width;
            xOffset *= Screen.width;
        }
        return new Rect(rect.x + xOffset, rect.y + yOffset, width, height * EditorGUIUtility.singleLineHeight);
    }
}
