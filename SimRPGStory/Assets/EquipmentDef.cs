﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;
using UnityEditorInternal;

[CanEditMultipleObjects]
[CustomEditor(typeof(EquipmentDef))]
public class EquipmentDefEditor : DefBase
{
    ReorderableList weapons;
    ReorderableList helmets;
    ReorderableList upperArmour;
    ReorderableList lowerArmour;
    ReorderableList gloves;
    ReorderableList boots;        

    public override void OnInspectorGUI()
    {
        EquipmentDef target = (EquipmentDef)this.target;

        GUILayout.Label("Filename");
        target.filename = GUILayout.TextField(target.filename);
        if (GUILayout.Button("Export"))
        {
            StreamWriter sw = StoryExporter.GetFile(StoryExporter.GetPath(target.filename));

            List<ExportItem> items = new List<ExportItem>();
            items.AddRange(ExportItem.GetItems(target.boots));
            items.AddRange(ExportItem.GetItems(target.gloves));
            items.AddRange(ExportItem.GetItems(target.helmet));
            items.AddRange(ExportItem.GetItems(target.lowerArmour));
            items.AddRange(ExportItem.GetItems(target.upperArmour));
            items.AddRange(ExportItem.GetItems(target.weapons));

            EquipmentExport export = new EquipmentExport() { items = items.ToArray() };
            
            Debug.Log(StoryExporter.WriteToFile(sw, export));            
        }

        DrawWeapons(target);
        DrawHelmets(target);
        DrawUpperArmour(target);
        DrawLowerArmour(target);
        DrawGloves(target);
        DrawBoots(target);

        serializedObject.ApplyModifiedProperties();
    }

    private void OnEnable()
    {     
        weapons = new ReorderableList(serializedObject, serializedObject.FindProperty("weapons"), true, true, true, true);                
        helmets = new ReorderableList(serializedObject, serializedObject.FindProperty("helmet"), true, true, true, true);
        upperArmour = new ReorderableList(serializedObject, serializedObject.FindProperty("upperArmour"), true, true, true, true);
        lowerArmour = new ReorderableList(serializedObject, serializedObject.FindProperty("lowerArmour"), true, true, true, true);
        gloves = new ReorderableList(serializedObject, serializedObject.FindProperty("gloves"), true, true, true, true);
        boots = new ReorderableList(serializedObject, serializedObject.FindProperty("boots"), true, true, true, true);
    }

    public int LabelLength(string text)
    {
        return 10 * text.Length;
    }

    public void DrawWeapons(EquipmentDef target)
    {
        weapons.elementHeightCallback = (int index) =>
        {
            if (!weapons.serializedProperty.isExpanded) return 0;
            float height = EditorGUIUtility.singleLineHeight * 1.5f;            
            return height;
        };

        weapons.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {            
            if (weapons.serializedProperty.isExpanded)
            {
                rect.y += 2;
                SerializedProperty element = weapons.serializedProperty.GetArrayElementAtIndex(index);
                SerializedProperty id = element.FindPropertyRelative("id");
                SerializedProperty cost = element.FindPropertyRelative("cost");
                SerializedProperty minDamage = element.FindPropertyRelative("minDamage");
                SerializedProperty maxDamage = element.FindPropertyRelative("maxDamage");
                EditorGUI.LabelField(RectOffset(rect, 0, 0, 30, 1, false), index.ToString());

                id.enumValueIndex = (int)(WeaponId)EditorGUI.EnumPopup(RectOffset(rect, 0.05f, 0, 0.3f, 1, true), target.weapons[index].id);

                EditorGUI.LabelField(RectOffset(rect, 0.37f, 0, 0.1f, 1, true), "Cost");
                cost.intValue = EditorGUI.IntField(RectOffset(rect, 0.43f, 0, 0.1f, 1, true), cost.intValue);

                EditorGUI.LabelField(RectOffset(rect, 0.56f, 0, 0.1f, 1, true), "Damage");
                Vector2 damage = EditorGUI.Vector2Field(RectOffset(rect, 0.66f, 0, 0.2f, 1, true), "", new Vector2(minDamage.intValue, maxDamage.intValue));
                minDamage.intValue = (int)damage.x;
                maxDamage.intValue = (int)damage.y;
            }          
        };
        weapons.serializedProperty.isExpanded = DrawItem(weapons, "Weapons", weapons.serializedProperty.isExpanded);
    }
    public void DrawHelmets(EquipmentDef target)
    {        
        helmets.elementHeightCallback = (int index) =>
        {
            if (!helmets.serializedProperty.isExpanded) return 0;
            float height = EditorGUIUtility.singleLineHeight * 1.5f;
            return height;
        };

        helmets.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            if (helmets.serializedProperty.isExpanded)
            {
                rect.y += 2;
                SerializedProperty element = helmets.serializedProperty.GetArrayElementAtIndex(index);
                SerializedProperty id = element.FindPropertyRelative("id");
                SerializedProperty cost = element.FindPropertyRelative("cost");
                SerializedProperty defense = element.FindPropertyRelative("defense");
                EditorGUI.LabelField(RectOffset(rect, 0, 0, 30, 1, false), index.ToString());

                id.enumValueIndex = (int)(HelmetId)EditorGUI.EnumPopup(RectOffset(rect, 0.05f, 0, 0.3f, 1, true), target.helmet[index].id);

                EditorGUI.LabelField(RectOffset(rect, 0.37f, 0, 0.1f, 1, true), "Cost");
                cost.intValue = EditorGUI.IntField(RectOffset(rect, 0.43f, 0, 0.1f, 1, true), cost.intValue);

                EditorGUI.LabelField(RectOffset(rect, 0.56f, 0, 0.1f, 1, true), "Defense");
                defense.intValue = EditorGUI.IntField(RectOffset(rect, 0.66f, 0, 0.1f, 1, true), defense.intValue);
            }
        };
        helmets.serializedProperty.isExpanded = DrawItem(helmets, "Helmets", helmets.serializedProperty.isExpanded);
    }
    public void DrawUpperArmour(EquipmentDef target)
    {
        upperArmour.elementHeightCallback = (int index) =>
        {
            if (!upperArmour.serializedProperty.isExpanded) return 0;
            float height = EditorGUIUtility.singleLineHeight * 1.5f;
            return height;
        };

        upperArmour.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            if (upperArmour.serializedProperty.isExpanded)
            {
                rect.y += 2;
                SerializedProperty element = upperArmour.serializedProperty.GetArrayElementAtIndex(index);
                SerializedProperty id = element.FindPropertyRelative("id");
                SerializedProperty cost = element.FindPropertyRelative("cost");
                SerializedProperty defense = element.FindPropertyRelative("defense");
                EditorGUI.LabelField(RectOffset(rect, 0, 0, 30, 1, false), index.ToString());

                id.enumValueIndex = (int)(UpperArmourId)EditorGUI.EnumPopup(RectOffset(rect, 0.05f, 0, 0.3f, 1, true), target.upperArmour[index].id);

                EditorGUI.LabelField(RectOffset(rect, 0.37f, 0, 0.1f, 1, true), "Cost");
                cost.intValue = EditorGUI.IntField(RectOffset(rect, 0.43f, 0, 0.1f, 1, true), cost.intValue);

                EditorGUI.LabelField(RectOffset(rect, 0.56f, 0, 0.1f, 1, true), "Defense");
                defense.intValue = EditorGUI.IntField(RectOffset(rect, 0.66f, 0, 0.1f, 1, true), defense.intValue);
            }
        };
        upperArmour.serializedProperty.isExpanded = DrawItem(upperArmour, "UpperArmour", upperArmour.serializedProperty.isExpanded);
    }
    public void DrawLowerArmour(EquipmentDef target)
    {
        lowerArmour.elementHeightCallback = (int index) =>
        {
            if (!lowerArmour.serializedProperty.isExpanded) return 0;
            float height = EditorGUIUtility.singleLineHeight * 1.5f;
            return height;
        };

        lowerArmour.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            if (lowerArmour.serializedProperty.isExpanded)
            {
                rect.y += 2;
                SerializedProperty element = lowerArmour.serializedProperty.GetArrayElementAtIndex(index);
                SerializedProperty id = element.FindPropertyRelative("id");
                SerializedProperty cost = element.FindPropertyRelative("cost");
                SerializedProperty defense = element.FindPropertyRelative("defense");
                EditorGUI.LabelField(RectOffset(rect, 0, 0, 30, 1, false), index.ToString());

                id.enumValueIndex = (int)(LowerArmourId)EditorGUI.EnumPopup(RectOffset(rect, 0.05f, 0, 0.3f, 1, true), target.lowerArmour[index].id);

                EditorGUI.LabelField(RectOffset(rect, 0.37f, 0, 0.1f, 1, true), "Cost");
                cost.intValue = EditorGUI.IntField(RectOffset(rect, 0.43f, 0, 0.1f, 1, true), cost.intValue);

                EditorGUI.LabelField(RectOffset(rect, 0.56f, 0, 0.1f, 1, true), "Defense");
                defense.intValue = EditorGUI.IntField(RectOffset(rect, 0.66f, 0, 0.1f, 1, true), defense.intValue);
            }
        };
        lowerArmour.serializedProperty.isExpanded = DrawItem(lowerArmour, "LowerArmour", lowerArmour.serializedProperty.isExpanded);
    }
    public void DrawGloves(EquipmentDef target)
    {
        gloves.elementHeightCallback = (int index) =>
        {
            if (!gloves.serializedProperty.isExpanded) return 0;
            float height = EditorGUIUtility.singleLineHeight * 1.5f;
            return height;
        };

        gloves.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            if (gloves.serializedProperty.isExpanded)
            {
                rect.y += 2;
                SerializedProperty element = gloves.serializedProperty.GetArrayElementAtIndex(index);
                SerializedProperty id = element.FindPropertyRelative("id");
                SerializedProperty cost = element.FindPropertyRelative("cost");
                SerializedProperty defense = element.FindPropertyRelative("defense");
                EditorGUI.LabelField(RectOffset(rect, 0, 0, 30, 1, false), index.ToString());

                id.enumValueIndex = (int)(GlovesId)EditorGUI.EnumPopup(RectOffset(rect, 0.05f, 0, 0.3f, 1, true), target.gloves[index].id);

                EditorGUI.LabelField(RectOffset(rect, 0.37f, 0, 0.1f, 1, true), "Cost");
                cost.intValue = EditorGUI.IntField(RectOffset(rect, 0.43f, 0, 0.1f, 1, true), cost.intValue);

                EditorGUI.LabelField(RectOffset(rect, 0.56f, 0, 0.1f, 1, true), "Defense");
                defense.intValue = EditorGUI.IntField(RectOffset(rect, 0.66f, 0, 0.1f, 1, true), defense.intValue);
            }
        };
        gloves.serializedProperty.isExpanded = DrawItem(gloves, "Gloves", gloves.serializedProperty.isExpanded);
    }
    public void DrawBoots(EquipmentDef target)
    {
        boots.elementHeightCallback = (int index) =>
        {
            if (!boots.serializedProperty.isExpanded) return 0;
            float height = EditorGUIUtility.singleLineHeight * 1.5f;
            return height;
        };

        boots.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            if (boots.serializedProperty.isExpanded)
            {
                rect.y += 2;
                SerializedProperty element = boots.serializedProperty.GetArrayElementAtIndex(index);
                SerializedProperty id = element.FindPropertyRelative("id");
                SerializedProperty cost = element.FindPropertyRelative("cost");
                SerializedProperty defense = element.FindPropertyRelative("defense");
                EditorGUI.LabelField(RectOffset(rect, 0, 0, 30, 1, false), index.ToString());

                id.enumValueIndex = (int)(BootsId)EditorGUI.EnumPopup(RectOffset(rect, 0.05f, 0, 0.3f, 1, true), target.boots[index].id);

                EditorGUI.LabelField(RectOffset(rect, 0.37f, 0, 0.1f, 1, true), "Cost");
                cost.intValue = EditorGUI.IntField(RectOffset(rect, 0.43f, 0, 0.1f, 1, true), cost.intValue);

                EditorGUI.LabelField(RectOffset(rect, 0.56f, 0, 0.1f, 1, true), "Defense");
                defense.intValue = EditorGUI.IntField(RectOffset(rect, 0.66f, 0, 0.1f, 1, true), defense.intValue);
            }
        };
        boots.serializedProperty.isExpanded = DrawItem(boots, "Boots", boots.serializedProperty.isExpanded);
    }
}

[System.Serializable]
public class EquipmentExport
{    
    public ExportItem[] items;    
}

[CreateAssetMenu(fileName = "EquipmentDef", menuName = "RPG/EquipmentDef")]
public class EquipmentDef : ScriptableObject {

    public string filename;

    public Weapon[] weapons;
    public Helmet[] helmet;
    public UpperArmour[] upperArmour;
    public LowerArmour[] lowerArmour;
    public Gloves[] gloves;
    public Boots[] boots;
}
