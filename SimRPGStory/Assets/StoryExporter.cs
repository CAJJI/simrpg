﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;

[CanEditMultipleObjects]
[CustomEditor(typeof(StoryExporter))]
public class StoryExporterEditor : Editor
{

    public override void OnInspectorGUI()
    {
        StoryExporter target = (StoryExporter)this.target;

        DrawDefaultInspector();

        if (GUILayout.Button("Export"))
            target.Export();
    }
}

[System.Serializable]
public class ExportStoreItemInfo
{
    public ItemType type;
    public int id;
}

[System.Serializable]
public class ExportStoreInfo
{
    public ExportStoreItemInfo[] items;    
}

[System.Serializable]
public class ExportBattleInfo
{
    public BattleInfo activeInfo;
    public BattleInfo[] infos;
}

[System.Serializable]
public class ExportState
{    
    public bool isStore;
    public bool isBattle;

    public Location location;
    public bool temporary;
    public int id;
    public string text;
    public List<string> responses = new List<string>();
    public List<int> destinationIds = new List<int>();

    public ExportStoreInfo store;
    public ExportBattleInfo battle;
}

[System.Serializable]
public class ExportData {

    public List<ExportState> states = new List<ExportState>();
}

public class StateInstance
{
    public State state;
    public ExportState storyState;
}

public class StoryExporter : StateBase {

    //List<StoryState> states = new List<StoryState>();
    List<StateInstance> examinedStates = new List<StateInstance>();
    ExportData export = new ExportData();
    public string filename;
    string path { get { return GetPath(filename); } }    

    public static string GetPath(string filename)
    {
        string path = Application.dataPath;
        string remove = "SimRPGStory/Assets/";        
        string ret = "";
        int length = path.Length - remove.Length;
        for (int i = 0; i < length; i++)
        {
            ret += path[i];
        }
        ret += "/data/" + filename + ".json";
        //ret = path + filename + ".json";
        return ret;
    }

    public static StreamWriter GetFile(string path)
    {
        Debug.Log(path);
        //if (!File.Exists(path))       
            //File.CreateText(path);                
        return new StreamWriter(path);
    }

    public static string WriteToFile(StreamWriter streamWriter, object data)
    {
        string output = JsonUtility.ToJson(data, true);
        streamWriter.Write(output);
        streamWriter.Dispose();        
        return output;
    }

    public static string GetName(object value, System.Type enumType)
    {
        string enumName = System.Enum.GetName(enumType, value);
        string name = "";
        int index = 0;
        foreach (char character in enumName.ToCharArray())
        {
            if (index > 0 && character == char.ToUpper(character))
                name += " ";
            name += character;
            index++;
        }
        return name;
    }

    public void Export()
    {
        StreamWriter sw = GetFile(path);
        
        export.states = new List<ExportState>();
        examinedStates = new List<StateInstance>();
        State state = GetAnimator().GetBehaviours<State>()[0];
        ExportState startStart = CreateState(state);
        AddState(state, startStart);
        
        Debug.Log(WriteToFile(sw, export));        
    }

    public ExportState CreateState(State state)
    {
        ExportState newState = new ExportState();
        newState.id = export.states.Count;
        newState.text = state.text;
        newState.temporary = state.temporary;
        newState.location = state.location;

        foreach (string response in state.responses)        
            newState.responses.Add(response);

        AnimatorState animatorState = state.GetState();
        
        foreach(StateMachineBehaviour behaviour in animatorState.behaviours)
        {
            if (behaviour is Store)
            {
                newState.isStore = true;
                Store store = behaviour as Store;
                newState.store = GetStore(store);
            }
            if (behaviour is BattleState)
            {
                BattleState battleState = behaviour as BattleState;
                newState.battle = GetBattleInfo(battleState);
            }
        }

        return newState;
    }

    public void AddState(State parentState, ExportState parent)
    {
        export.states.Add(parent);
        examinedStates.Add(new StateInstance() { state = parentState, storyState = parent });

        AnimatorStateTransition[] transitions = parentState.GetState().transitions;
        foreach (AnimatorStateTransition transition in transitions)
        {
            State childState = GetBehaviour<State>(transition.destinationState);
            if (!childState) continue;
            StateInstance instance = GetStateInstance(childState);
            if (instance == null)
            {                
                ExportState child = CreateState(childState);
                parent.destinationIds.Add(child.id);
                AddState(childState, child);
            }
            else
                parent.destinationIds.Add(instance.storyState.id);
        }
    }

    public ExportBattleInfo GetBattleInfo(BattleState battleState)
    {
        return new ExportBattleInfo
        {
            infos = battleState.infos
        };
    }

    public ExportStoreInfo GetStore(Store store)
    {
        List<ExportStoreItemInfo> items = new List<ExportStoreItemInfo>();
        foreach (WeaponId item in store.weapons)
            items.Add(new ExportStoreItemInfo() { type = ItemType.Weapon, id = (int)item });
        foreach (HelmetId item in store.helmets)
            items.Add(new ExportStoreItemInfo() { type = ItemType.Helmet, id = (int)item });
        foreach (UpperArmourId item in store.uppers)
            items.Add(new ExportStoreItemInfo() { type = ItemType.UpperArmour, id = (int)item });
        foreach (LowerArmourId item in store.lowers)
            items.Add(new ExportStoreItemInfo() { type = ItemType.LowerArmour, id = (int)item });
        foreach (GlovesId item in store.gloves)
            items.Add(new ExportStoreItemInfo() { type = ItemType.Gloves, id = (int)item });
        foreach (BootsId item in store.boots)
            items.Add(new ExportStoreItemInfo() { type = ItemType.Boots, id = (int)item });

        return new ExportStoreInfo
        {
            items = items.ToArray()
        };
    }

    StateInstance GetStateInstance(State state)
    {
        foreach(StateInstance instance in examinedStates)
        {
            if (instance.state == state)
                return instance;
        }
        return null;
    }

    public T GetBehaviour<T>(AnimatorState state) where T : StateMachineBehaviour
    {
        foreach(StateMachineBehaviour behaviour in state.behaviours)
        {
            if (behaviour is T)
                return behaviour as T;
        }
        return null;
    }
}
