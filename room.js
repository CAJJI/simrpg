const tools = require('./tools.js');

var methods ={

    GetUserId: function(text){
        return text.substring(text.lastIndexOf("@") + 1, text.lastIndexOf(">"));        
    },
    
    GetUsername: function (id) {
        return "<@" + id + ">";
    },    
    
    SendMessage: function (msg, text, boxText) {
        
        if (boxText)            
            text = tools.BoxText(text);        
        if (text){
            
            msg.channel.send(text);
        }
    },    
};

module.exports = methods;