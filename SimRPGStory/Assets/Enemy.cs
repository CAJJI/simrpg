﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyId
{
    Goblin,
    Hobgoblin,
}

[System.Serializable]
public class Enemy {
    public EnemyId id;
    public string name;
    public int maxHealth;
    public int health;
    public int minDamage;
    public int maxDamage;
	
}
